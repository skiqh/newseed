/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
	// Application Constructor
	initialize: function() {
		this.bindEvents();
	},
	// Bind Event Listeners
	//
	// Bind any events that are required on startup. Common events are:
	// 'load', 'deviceready', 'offline', and 'online'.
	bindEvents: function() {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},

	// deviceready Event Handler
	//
	// The scope of 'this' is the event. In order to call the 'receivedEvent'
	// function, we must explicitly call 'app.receivedEvent(...);'
	onDeviceReady: function() {
		// app.receivedEvent('deviceready');
		console.log("device ready")
		// navigator.vibrate(3000)
		// navigator.notification.beep(1)

		var statusElement = document.getElementById('status')
		var seqElement = document.getElementById('seq')
		statusElement.innerHTML = 'loading...'

		window.mypouch = new PouchDB('mosambique')
		var data_source_url =  'https://ecrc.cloudant.com/dfm'
		window.remotedb = new PouchDB(data_source_url)

		function update_seq_info() {
			Promise.all(
				[	window.mypouch.info()
				,	window.remotedb.info()
				])
			.then(function(result) {
				// console.log("result" + JSON.stringify(result, null, '\t'))
				seqElement.innerHTML = result[0].update_seq + '/' + parseInt(result[1].update_seq)
			})
		}
		update_seq_info()

		window.sync = PouchDB.sync(window.mypouch, window.remotedb, {
			  retry: true,
			   live: true
			})
		window.sync.on('change', function(update) {
				// console.log("update" + JSON.stringify(update, null, '\t'))
				// update_seq_info(update.change.last_seq)
				update_seq_info()
				statusElement.innerHTML = 'change'

				// statusElement.innerHTML = JSON.stringify(update.change, null, '\t')
			})
			.on('paused', function (err) {
				if(err)
					console.log("paused:\n" + JSON.stringify(err, null, '\t'))
				else
					console.log("paused")
				update_seq_info()
				statusElement.innerHTML = 'paused'
			})
			.on('active', function () {
				console.log("active")
				update_seq_info()
				statusElement.innerHTML = 'active'
			})
			.on('denied', function (err) {
				console.log("denied:\n" + JSON.stringify(err, null, '\t'))
				update_seq_info()
				statusElement.innerHTML = 'denied'
			})
			.on('complete', function (info) {
				statusElement.innerHTML = 'complete'
				update_seq_info()
				console.log("complete:\n" + JSON.stringify(info, null, '\t'))
			})
			.on('error', function (err) {
				statusElement.innerHTML = 'error'
				update_seq_info()
				console.log("error:\n" + JSON.stringify(err, null, '\t'))
			})

		riot.route('/', function() {
			console.log("route /")
			riot.mount('#content', 'list-docs-simple')
		})

		riot.route('/article/*', function(article_id) {
			console.log("route /article/'" + article_id)
			riot.mount('#content', 'show-article', {article_id:article_id})
		})
		riot.route('/frame/*', function(article_id) {
			console.log("route /frame/'" + article_id)
			riot.mount('#content', 'show-frame', {article_id:article_id})
		})
		riot.route.start(true)


		// We must wait for the "deviceready" event to fire
		// before we can use the store object.

		// function initializeStore() {
		// 	console.log("store" + JSON.stringify(store, null, '\t'))
		//     // Let's set a pretty high verbosity level, so that we see a lot of stuff
		//     // in the console (reassuring us that something is happening).
		//     store.verbosity = store.INFO;

		//     // We register a dummy product. It's ok, it shouldn't
		//     // prevent the store "ready" event from firing.
		//     store.register({
		//         id:    "com.example.app.inappid1",
		//         alias: "100 coins",
		//         type:  store.CONSUMABLE
		//     });

		//     // When every goes as expected, it's time to celebrate!
		//     // The "ready" event should be welcomed with music and fireworks,
		//     // go ask your boss about it! (just in case)
		//     store.ready(function() {
		//         console.log("\\o/ STORE READY \\o/");
		//     });

		//     // After we've done our setup, we tell the store to do
		//     // it's first refresh. Nothing will happen if we do not call store.refresh()
		//     store.refresh();
		// }
		// initializeStore()

		// var display_all_docs = debounce(function() {
		//     mypouch.allDocs().then(function(result) {
		//         console.log("result", result)
		//         statusElement.innerHTML = JSON.stringify(result.docs, null, '\t')

		//     })
		// }, 300)




		// var changes = db.changes({
		//     since: 'now',
		//     live: true,
		//     include_docs: true
		// }).on('change', ).on('complete', function(info) {
		//     // changes() was canceled
		// }).on('error', function (err) {
		//     console.log(err);
		// });
		// console.log('vibrated.')



		// // onLocationSuccess Callback
		// // This method accepts a Position object, which contains the
		// // current GPS coordinates
		// //
		// var onLocationSuccess = function(position) {
		//     var elm = document.getElementById('status')
		//     console.log("elm" + JSON.stringify(elm, null, '\t'))

		//     status =
		//           'Latitude:          ' + position.coords.latitude
		//        +'\nLongitude:         ' + position.coords.longitude
		//        +'\nAltitude:          ' + position.coords.altitude
		//        +'\nAccuracy:          ' + position.coords.accuracy
		//        +'\nAltitude Accuracy: ' + position.coords.altitudeAccuracy
		//        +'\nHeading:           ' + position.coords.heading
		//        +'\nSpeed:             ' + position.coords.speed
		//        +'\nTimestamp:         ' + position.timestamp
		//     statusElement.innerHTML = status
		//     // alert('Latitude: '          + position.coords.latitude          + '\n' +
		//     //       'Longitude: '         + position.coords.longitude         + '\n' +
		//     //       'Altitude: '          + position.coords.altitude          + '\n' +
		//     //       'Accuracy: '          + position.coords.accuracy          + '\n' +
		//     //       'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
		//     //       'Heading: '           + position.coords.heading           + '\n' +
		//     //       'Speed: '             + position.coords.speed             + '\n' +
		//     //       'Timestamp: '         + position.timestamp                + '\n');
		// };

		// // onError Callback receives a PositionError object
		// //
		// function onError(error) {
		//     alert('code: '    + error.code    + '\n' +
		//           'message: ' + error.message + '\n');
		// }

		// navigator.geolocation.getCurrentPosition(onLocationSuccess, onError);




	},
	// Update DOM on a Received Event
	receivedEvent: function(id) {
		var parentElement = document.getElementById(id);
		var listeningElement = parentElement.querySelector('.listening');
		var receivedElement = parentElement.querySelector('.received');

		listeningElement.setAttribute('style', 'display:none;');
		receivedElement.setAttribute('style', 'display:block;');

		console.log('Received Event: ' + id);



		// console.log("window.geofence", window.geofence)
		// window.geofence.initialize().then(function () {
		//         console.log("Successful initialization");
		//     }, function (error) {
		//         console.log("Error", error);
		//     })

	}
};


// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

PouchDB.attachmentBlobUrl = function(doc, attachment_id) {
	if(!doc)
		throw "now doc"
	if(!attachment_id)
		throw doc._id + "no attachment_id"
	if(!doc._attachments)
		throw doc._id + "no _attachments"
	if(!(attachment_id in doc._attachments))
		throw doc._id + "attachment_id '"+attachment_id+"' not in doc._attachments (" + Object.keys(doc._attachments).join(',') + ")"
	if(!doc._attachments[attachment_id].data)
		throw doc._id + "no data for attachment_id '"+attachment_id+"' in doc._attachments"
	return URL.createObjectURL(doc._attachments[attachment_id].data)
}

window.decodeEntities = (function() {
  // this prevents any overhead from creating the object each time
  var element = document.createElement('div');

  function decodeHTMLEntities (str) {
    if(str && typeof str === 'string') {
      // strip script/html tags
      str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
      str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
      element.innerHTML = str;
      str = element.textContent;
      element.textContent = '';
    }

    return str;
  }

  return decodeHTMLEntities;
})();