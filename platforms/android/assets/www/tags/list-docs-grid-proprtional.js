
riot.tag2('list-docs', '<div class="wrapper-outer wrapper-outer-1-1" each="{row in rows}"> <div class="wrapper-inner"><a class="article" riot-href="#/article/{row.id}"> <div class="header" riot-style="background-image: url(&quot;{row.cover_img_url}&quot;); background-position: {row.doc.cover_position || &quot;center&quot;}"> <div class="veil" riot-style="background-color: rgba(0,0,0,{row.doc.cover_veil || \'0.3\'})"></div> <div class="headlines"> <div class="kicker">{decodeEntities(row.doc.kicker)}</div> <div class="headline">{decodeEntities(row.doc.headline)}</div> <div class="author">{decodeEntities(row.doc.author)}</div> </div> </div> <div class="teaser">{decodeEntities(row.doc.teaser || ⁗&nbsp;⁗)}</div></a></div> </div>', '.list-docs { width: 100%; -webkit-column-width: 20em; -webkit-column-gap: 2em; -webkit-column-rule: 1px solid #eee; -moz-column-width: 20em; -moz-column-gap: 2em; -moz-column-rule: 1px solid #eee; -ms-column-width: 20em; -ms-column-gap: 2em; -ms-column-rule: 1px solid #eee; column-width: 20em; column-gap: 2em; column-rule: 1px solid #eee; -webkit-column-count: 1; -moz-column-count: 1; -ms-column-count: 1; column-count: 1; } @media screen and (min-width: 50em) { .list-docs { -webkit-column-count: 2; -moz-column-count: 2; -ms-column-count: 2; column-count: 2; } } @media screen and (min-width: 100em) { .list-docs { -webkit-column-count: 3; -moz-column-count: 3; -ms-column-count: 3; column-count: 3; } } @media screen and (min-width: 150em) { .list-docs { -webkit-column-count: 4; -moz-column-count: 4; -ms-column-count: 4; column-count: 4; } } .wrapper-outer { width: 100%; display: inline-block; position: relative; break-inside: avoid-column; -webkit-column-break-inside: avoid; page-break-inside: avoid; overflow: hidden; } .wrapper-outer:after { display: block; content: \'\'; } .wrapper-outer-16-9 { padding-top: 56.25%; } .wrapper-outer-3-4 { padding-top: 75%; } .wrapper-outer-1-1 { padding-top: 100%; } .wrapper-inner { position: absolute; top: 0; bottom: 0; right: 0; left: 0; } .list-docs a.article { text-decoration: none; color: inherit; margin-bottom: 3em; } .list-docs .header { position: relative; width: 100%; height: 300px; overflow: hidden; background-size: cover; background-repeat: no-repeat; } .list-docs .veil { background-color: rgba(0,0,0,0.3); position: absolute; top: 0; bottom: 0; left: 0; right: 0; } .list-docs .headlines { padding: 1em; position: absolute; bottom: 0; left: 0; right: 0; padding: 1em; color: #fff; text-shadow: 0 0 3px rgba(0,0,0,0.8); } .list-docs .kicker { } .list-docs .headline { font-size: x-large; text-transform: uppercase; } .list-docs .author { font-size: xx-small; font-style: italic; } .list-docs .teaser { padding: 1em; font-size: 8pt !important; }', 'class="list-docs row"', function(opts) {
    var tag = this

    tag.recipient = "white iphone"

    tag.opts.sync.on('pause', function() {
    	tag.refresh()
    })
    tag.refresh = debounce(function() {

    	tag.mypouch = tag.opts.mypouch
    	tag.mypouch.query('articles/by_date', {include_docs:true, attachments:true, binary:true})
    		.then( function(result) {
    			tag.rows = result.rows.map(function(row) {

    				if(row.doc.cover_img)
    					row.cover_img_url = PouchDB.attachmentBlobUrl(row.doc, row.doc.cover_img)

    				return row
    			})
    			tag.update()
    			console.log(result)

    		})
    	tag.mypouch.allDocs({include_docs:true}).then(function(result) {
    		tag.alldocs = result
    		tag.update()
    	})
    }, 200)
    tag.refresh()
});