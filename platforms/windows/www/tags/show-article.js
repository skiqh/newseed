
riot.tag2('show-article', '<div class="header" riot-style="background-image: url(&quot;{article._cover_img.src}&quot;); background-position: {article.cover_position || &quot;center&quot;}" onclick="{refresh}"> <div class="veil"></div> <div class="headlines"> <div class="kicker">{decodeEntities(article.kicker)}</div> <div class="headline">{decodeEntities(article.headline)}</div> <div class="source">{decodeEntities(article.source)}</div> </div> </div> <div class="content"> <div class="teaser">{decodeEntities(article.teaser)}</div> <div class="text">{decodeEntities(article.text)}</div><a class="link" href="#/frame/{article._id}">Zum Artikel bei {article.source}</a> </div>', '.show-article { max-width: 50rem; margin: 2em auto; } .show-article .header { position: relative; width: 100%; height: 300px; overflow: hidden; background-size: cover; background-repeat: no-repeat; margin-bottom: 1em; } .show-article .veil { background-color: rgba(0,0,0,0.5); position: absolute; top: 0; bottom: 0; left: 0; right: 0; } .show-article .headlines { padding: 1rem; position: absolute; bottom: 0; left: 0; right: 0; text-shadow: 0 0 3px rgba(0,0,0,0.8); } .show-article .kicker { color: #fff; } .show-article .headline { font-family: \'LatoLatinWebBlack\'; color: #fff; font-size: x-large; } .show-article .source { color: #fff; font-size: xx-small; font-style: italic; } .content { padding: 0 1rem; } .show-article .teaser { font-weight: bold; font-size: 14pt; line-height: 1.5; margin-bottom: 2em; } .show-article .text { white-space: pre-wrap; font-family: Georgia, serif; font-size: 14pt; line-height: 1.5; margin-bottom: 1em; } .show-article video { max-width: 100%; }', 'class="show-article"', function(opts) {
    var tag = this
    tag.refresh = function() {

    	window.mypouch.get(tag.opts.article_id)

    		.then(function(doc) {
    			if(doc.videos) {
    				return Promise.all(doc.videos.map(function(filename) {
    					if(doc._attachments && (filename in doc._attachments))
    						return window.mypouch.getAttachment(doc._id, filename).then(function(blob) {
    							return {src: URL.createObjectURL(blob), type: doc._attachments[filename].content_type}

    						})
    					else
    						return
    					})).then(function(vids) {
    						console.log("vids" + JSON.stringify(vids, null, '\t'))
    						doc._videos = vids
    						return doc
    					})
    				}
    			else
    				return doc
    		})
    		.then(function(doc) {
    			if(doc.cover_img)
    				return window.mypouch.getAttachment(doc._id, doc.cover_img).then(function(blob) {
    					return {src: URL.createObjectURL(blob), type: doc._attachments[doc.cover_img].content_type}
    				}).then(function(cover_img) {
    					doc._cover_img = cover_img
    					return doc
    				})
    			else
    				return doc
    		})
    		.then(function(doc) {

    			tag.article = doc

    			tag.update()
    		})

    }
    window.sync
    	.on('change', function(update) {
    		if(tag.article && update.change && update.change.docs && update.change.docs.filter(function(doc) { return doc._id == tag.article._id }).length > 0)
    			tag.refresh()
    		console.log("update" + JSON.stringify(update.change.docs.map(function(doc) {
    			return doc._id
    		}), null, '\t'))

    	})

    tag.refresh()

    tag.submitpayment = function() {
    	tag.stripe_submit.disabled = true
    	var stripe_data =
    		{	number: tag.stripe_number.value
    		,	cvc: tag.stripe_cvc.value
    		,	exp_month: tag.stripe_exp_month.value
    		,	exp_year: tag.stripe_exp_year.value
    		}
    	console.log("stripe_data" + JSON.stringify(stripe_data, null, '\t'))
    	Stripe.card.createToken(stripe_data, function(status, response) {

    		console.log("status" + JSON.stringify(status, null, '\t'))
    		console.log("response" + JSON.stringify(response, null, '\t'))

    	})
    }
});