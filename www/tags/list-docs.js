
riot.tag2('list-docs', '<div class="wrapper-outer wrapper-outer-1-1" each="{row in rows}" riot-style="flex-grow: {(row.doc.weight || 1)}; flex-basis: {(row.doc.weight || 1)*25 + (row.doc.weight || 0)}em"> <div class="wrapper-inner"><a class="article" riot-href="#/article/{row.id}"> <div class="header" riot-style="background-image: url(&quot;{row._cover_img}&quot;); background-position: {row.doc.cover_position || &quot;center&quot;}"> <div class="veil" riot-style="background-color: rgba(0,0,0,{row.doc.cover_veil || \'0.3\'})"></div> <div class="headlines"> <div class="kicker">{decodeEntities(row.doc.kicker)}</div> <div class="headline">{decodeEntities(row.doc.headline)}</div> <div class="author">{decodeEntities(row.doc.author)}</div> </div> </div> <div class="teaser">{decodeEntities(row.doc.teaser || row.doc.text.split(\' \').slice(0, 28).join(\' \') + \'&nbsp;&hellip;\' || ⁗&nbsp;⁗)}</div></a></div> </div>', '.list-docs { width: 100%; display: flex; flex-direction: row; flex-wrap: wrap; justify-content: flex-start; align-items: stretch; align-content: flex-start; margin-right: -2.5em; } .wrapper-outer { margin-right: 2.5em; margin-bottom: 2.5em; flex-grow: 1; } .list-docs a.article { text-decoration: none; color: inherit; margin-bottom: 3em; } .list-docs .header { position: relative; width: 100%; height: 300px; overflow: hidden; background-size: cover; background-repeat: no-repeat; } .list-docs .veil { background-color: rgba(0,0,0,0.3); position: absolute; top: 0; bottom: 0; left: 0; right: 0; } .list-docs .headlines { padding: 1em; position: absolute; bottom: 0; left: 0; right: 0; padding: 1em; color: #fff; text-shadow: 0 0 3px rgba(0,0,0,0.8); } .list-docs .kicker { } .list-docs .headline { font-family: \'LatoLatinWebBlack\'; font-weight: 900; font-size: x-large; } .list-docs .author { font-family: serif; font-size: xx-small; font-style: italic; font-weight: 100; } .list-docs .teaser { padding: 1em; font-size: 8pt !important; }', 'class="list-docs row"', function(opts) {
    var tag = this

    tag.recipient = "white iphone"

    window.sync.on('pause', function() {
    	console.log("list-docs db paused")
    	tag.refresh()
    })
    tag.refresh = debounce(function() {

    	tag.mypouch = window.mypouch
    	tag.mypouch.query('articles/by_date', {include_docs:true, descending:true, limit:12 })
    		.then( function(result) {

    			return Promise.all(result.rows.map(function (row) {
    				if(row.doc.cover_img && row.doc._attachments && (row.doc.cover_img in row.doc._attachments))
    					return tag.mypouch.getAttachment(row.id, row.doc.cover_img).then(function(blobOrBuffer) {

    						row._cover_img = URL.createObjectURL(blobOrBuffer)
    						return row
    					})
    				else
    					return row
    			}))
    		})
    		.then(function(rows) {

    			tag.rows = rows
    			tag.update()
    		})
    		.catch(function(err) {
    			console.error("getAttachment Promise Error" + JSON.stringify(err, null, '\t'))

    		})

    }, 200)
    tag.refresh()
});