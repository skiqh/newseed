
riot.tag2('show-frame', '<div class="loader" show="{!ready}"></div> <div class="header" riot-style="background-image: url(&quot;{article._cover_img.src}&quot;); background-position: {article.cover_position || &quot;center&quot;}" onclick="{refresh}" hide="{!ready}"> <div class="veil"></div> <div class="headlines"> <div class="kicker">{decodeEntities(article.kicker)}</div> <div class="headline">{decodeEntities(article.headline)}</div> <div class="source"> Original bei&nbsp;<a class="link" href="{article.url}" target="_blank">{decodeEntities(article.source)}</a></div> </div> </div> <div class="content" id="contenttag" hide="{!ready}"></div>', '.show-frame { max-width: 50rem; margin: 2em auto; } .show-frame .header { position: relative; width: 100%; height: 300px; overflow: hidden; background-size: cover; background-repeat: no-repeat; margin-bottom: 1em; } .show-frame .veil { background-color: rgba(0,0,0,0.5); position: absolute; top: 0; bottom: 0; left: 0; right: 0; } .show-frame .headlines { padding: 1rem; position: absolute; bottom: 0; left: 0; right: 0; text-shadow: 0 0 3px rgba(0,0,0,0.8); } .show-frame .kicker { color: #fff; } .show-frame .headline { font-family: \'LatoLatinWebBlack\'; color: #fff; font-size: x-large; } .show-frame .source { color: #fff; font-style: italic; } .link { color: inherit; } .link:hover { color: inherit; } .content { padding: 0 1rem; font-family: Georgia, serif; font-size: 14pt; line-height: 1.5; } .loader, .loader:before, .loader:after { border-radius: 50%; width: 2.5em; height: 2.5em; -webkit-animation-fill-mode: both; animation-fill-mode: both; -webkit-animation: load7 1.8s infinite ease-in-out; animation: load7 1.8s infinite ease-in-out; } .loader { color: #ffffff; font-size: 10px; margin: 80px auto; position: relative; text-indent: -9999em; -webkit-transform: translateZ(0); -ms-transform: translateZ(0); transform: translateZ(0); -webkit-animation-delay: -0.16s; animation-delay: -0.16s; } .loader:before, .loader:after { content: \'\'; position: absolute; top: 0; } .loader:before { left: -3.5em; -webkit-animation-delay: -0.32s; animation-delay: -0.32s; } .loader:after { left: 3.5em; } @-webkit-keyframes load7 { 0%, 80%, 100% { box-shadow: 0 2.5em 0 -1.3em; } 40% { box-shadow: 0 2.5em 0 0; } } @keyframes load7 { 0%, 80%, 100% { box-shadow: 0 2.5em 0 -1.3em; } 40% { box-shadow: 0 2.5em 0 0; } }', 'class="show-frame"', function(opts) {
    var tag = this
    tag.ready = false

    tag.refresh = function() {
    	window.mypouch.get(tag.opts.article_id)
    		.then(function(doc) {
    			if(doc.cover_img)
    				return window.mypouch.getAttachment(doc._id, doc.cover_img).then(function(blob) {
    					return {src: URL.createObjectURL(blob), type: doc._attachments[doc.cover_img].content_type}
    				}).then(function(cover_img) {
    					doc._cover_img = cover_img
    					return doc
    				})
    			else
    				return doc
    		})
    		.then(function(doc) {
    			tag.article = doc

    			var readable_url = 'http://readable-proxy.herokuapp.com/api/get?url=' + tag.article.url.replace(/^https?:\/\/internal.diefreiemeinung.net\/out\?url=(https?%3A%2F%2F[^&]+)/, function(_, encodedurl) {
    				return decodeURIComponent(encodedurl)
    			})

    			return goget(readable_url)
    		})
    		.catch(function(err) {
    			console.error("err" + JSON.stringify(err, null, '\t'))
    		})
    		.then(function(result) {

    			tag.contenttag.innerHTML = result.content
    			tag.ready = true
    			tag.update()
    		})
    }

    tag.refresh()

    function goget(url) {
    	return new Promise(function(resolve, reject) {
    		var request = new XMLHttpRequest()
    		request.open('GET', url, true)

    		request.onload = function() {

    			if (request.status >= 200 && request.status < 400) {

    				try {
    					var data = JSON.parse(request.responseText)
    					resolve(data)
    				}
    				catch(parse_json_ex) {
    					reject(parse_json_ex)
    				}
    			} else {
    				reject("We reached our target server, but it returned an error")
    			}
    		}
    		request.onerror = function() {
    			console.log(request.responseText)
    			reject("There was a connection error of some sort")
    		}
    		request.send()
    	})
    }
});