
riot.tag2('list-docs-simple', '<div class="wrapper-outer wrapper-outer-16-9" each="{row in rows}"> <div class="wrapper-inner"><a class="article" riot-href="#/frame/{row.id}"> <div class="header" riot-style="background-image: url(&quot;{row._cover_img}&quot;); background-position: {row.doc.cover_position || &quot;center&quot;}"> <div class="veil" riot-style="background-color: rgba(0,0,0,{row.doc.cover_veil || \'0.5\'})"></div> <div class="headlines"> <div class="kicker">{decodeEntities(row.doc.kicker)}</div> <div class="headline">{decodeEntities(row.doc.headline)}</div> <div class="teaser">{decodeEntities(row.doc.teaser || row.doc.text.split(\' \').slice(0, 28).join(\' \') + \'&nbsp;&hellip;\' || ⁗&nbsp;⁗)}</div> <div class="source">{decodeEntities(row.doc.source)}</div> </div> </div></a></div> </div>', '.wrapper-outer { width: 100%; display: inline-block; position: relative; break-inside: avoid-column; -webkit-column-break-inside: avoid; page-break-inside: avoid; overflow: hidden; margin-bottom: 3em; } .wrapper-outer:after { display: block; content: \'\'; } @media all and (orientation: landscape) { .wrapper-outer-16-9 { padding-top: 56.25%; } .wrapper-outer-3-4 { padding-top: 75%; } .wrapper-outer-1-1 { padding-top: 100%; } } @media all and (orientation: portrait) { .wrapper-outer-16-9 { padding-top: 177.7777%; } .wrapper-outer-3-4 { padding-top: 25%; } .wrapper-outer-1-1 { padding-top: 100%; } } .wrapper-inner { position: absolute; top: 0; bottom: 0; right: 0; left: 0; } .list-docs-simple { max-width: 50rem; margin: 0 auto; } //- .wrapper-outer { //- margin-bottom: 2.5em; //- width: 100%; //- } .list-docs-simple a.article { text-decoration: none; color: inherit; } .list-docs-simple .header { position: relative; width: 100%; //- height: 32em; height: 100%; overflow: hidden; background-size: cover; background-repeat: no-repeat; } .list-docs-simple .veil { background-color: rgba(0,0,0,0.3); position: absolute; top: 0; bottom: 0; left: 0; right: 0; } .list-docs-simple .headlines { padding: 1em; position: absolute; bottom: 0; left: 0; right: 0; padding: 1em; color: #fff; text-shadow: 0 0 3px rgba(0,0,0,0.8); } .list-docs-simple .kicker { } .list-docs-simple .headline { font-family: \'LatoLatinWebBlack\'; font-weight: 900; font-size: x-large; } .list-docs-simple .source { font-size: 10pt !important; font-family: serif; font-style: italic; font-weight: 100; } .list-docs-simple .teaser { font-size: 10pt !important; margin: 0.2em 0 0.2em 0; font-family: serif; }', 'class="list-docs-simple row"', function(opts) {
    var tag = this

    tag.recipient = "white iphone"

    window.sync.on('pause', function() {
    	console.log("list-docs-simple db paused")
    	tag.refresh()
    })
    tag.refresh = debounce(function() {

    	tag.mypouch = window.mypouch
    	tag.mypouch.query('articles/by_date', {include_docs:true, descending:true, limit:12 })
    		.then( function(result) {

    			return Promise.all(result.rows.map(function (row) {
    				if(row.doc.cover_img && row.doc._attachments && (row.doc.cover_img in row.doc._attachments))
    					return tag.mypouch.getAttachment(row.id, row.doc.cover_img).then(function(blobOrBuffer) {

    						row._cover_img = URL.createObjectURL(blobOrBuffer)
    						return row
    					})
    				else
    					return row
    			}))
    		})
    		.then(function(rows) {

    			tag.rows = rows
    			tag.update()
    		})
    		.catch(function(err) {
    			console.error("getAttachment Promise Error" + JSON.stringify(err, null, '\t'))

    		})

    }, 200)
    tag.refresh()
});